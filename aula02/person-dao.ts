import { Person } from './../aula01/person';
import { DaoInterface } from "./dao.interface"

export class PersonDao implements DaoInterface{
    tableName: string;

    insert(person: Person): boolean{
        console.log('insert:', person.toString());
        return true;
    }
    update(person: Person): boolean{
        console.log('update');
        return true;
    }
    delete(id: number): boolean{
        console.log('delete');
        return true;
    }
    find(id: number): Person{
        return null;
    }
    findAll(): [Person]{
        return [new Person('João')];
    }
}