import { DaoInterface } from './dao.interface';
import { Person } from '../aula01/person';
import { Dao } from './dao';

let dao: DaoInterface<Person> = new Dao<Person>();
let person: Person = new Person('José');

dao.insert(person);