import { Person } from './../aula01/person';
import { DaoInterface } from "./dao.interface"

export class Dao<T> implements DaoInterface<T>{
    tableName: string;

    insert(object: T): boolean{
        console.log('insert...');
        return true;
    }
    update(object: T): boolean{
        console.log('update');
        return true;
    }
    delete(id: number): boolean{
        console.log('delete');
        return true;
    }
    find(id: number): T{
        return null;
    }
    findAll(): [T]{
        return [null];
    }
}